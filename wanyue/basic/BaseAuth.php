<?php
// +----------------------------------------------------------------------
// |万岳科技开源系统 [山东万岳信息科技有限公司]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2022 https://www.sdwanyue.com All rights reserved.
// +----------------------------------------------------------------------
// | 万岳科技相关开源系统，需标注"代码来源于万岳科技开源项目"后即可免费自用运营，前端运营时展示的内容不得使用万岳科技相关信息
// +----------------------------------------------------------------------
// | Author: 万岳科技开源官方 < wanyuekj2020@163.com >
// +---------------------------------------------------------------------- 

namespace wanyue\basic;

use app\Request;

abstract class BaseAuth extends BaseStorage
{

    /**
     * 获取当前句柄名
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 登陆
     * @param Request $request
     * @return mixed
     */
    abstract public function login(Request $request);

    /**
     * 退出登陆
     * @param Request $request
     * @return mixed
     */
    abstract public function logout(Request $request);
}