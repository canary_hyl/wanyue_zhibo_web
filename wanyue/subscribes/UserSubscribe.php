<?php

namespace wanyue\subscribes;

use app\models\user\User;
use app\models\user\WechatUser;
use think\facade\Cookie;
use app\admin\model\system\SystemAttachment;

/**
 * 用户事件
 * Class UserSubscribe
 * @package wanyue\subscribes
 */
class UserSubscribe
{

    public function handle()
    {

    }

    /**
     * 管理员后台给用户添加金额
     * @param $event
     */
    public function onAdminAddMoney($event)
    {
        list($user, $money) = $event;
        //$user 用户信息
        //$money 添加的金额
    }

    /**
     * 用户访问记录
     * @param $event
     */
    public function onInitLogin($event)
    {
        list($userInfo) = $event;
        $request = app('request');
        User::edit(['last_time' => time(), 'last_ip' => $request->ip()], $userInfo->uid, 'uid');
    }


}