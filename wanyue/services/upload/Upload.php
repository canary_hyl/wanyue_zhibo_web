<?php

namespace wanyue\services\upload;

use wanyue\basic\BaseManager;
use think\facade\Config;

/**
 * Class Upload
 * @package wanyue\services\upload
 * @mixin \wanyue\services\upload\storage\Local
 * @mixin \wanyue\services\upload\storage\OSS
 * @mixin \wanyue\services\upload\storage\COS
 * @mixin \wanyue\services\upload\storage\Qiniu
 */
class Upload extends BaseManager
{
    /**
     * 空间名
     * @var string
     */
    protected $namespace = '\\wanyue\\services\\upload\\storage\\';

    /**
     * 设置默认上传类型
     * @return mixed
     */
    protected function getDefaultDriver()
    {
        return Config::get('upload.default', 'local');
    }


}