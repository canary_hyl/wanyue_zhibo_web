

> <div align=center><img src="https://images.gitee.com/uploads/images/2021/1018/173731_d689ecb0_8162876.png" width="590" height="212"/></div>
<div align="center">
 
[![](https://img.shields.io/badge/%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3-%E7%82%B9%E5%87%BB%E6%9F%A5%E7%9C%8B-yellow)](https://www.kancloud.cn/wanyuekaiyuan11/wanyue_zhibo_ios/2789011)
[![](https://img.shields.io/badge/QQ%E7%BE%A4-995910672-green)](https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi)
</div>


> ### 近期更新
> - 重新打包上架了Android应用演示
> 
> ### 项目说明（如果对你有用，请给个star！注意请点击上方“部署文档”查看部署文档，进行部署）
> ##### <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue_zhibo_ios/2789011">项目文档</a> |  <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue_zhibo_ios/2789011">部署文档</a>  |  <a target="_blank" href="https://www.kancloud.cn/wanyuekeji/wanyue_open_zhibo/2479722">常见问题</a> 
> ---
> 
> ### 系统演示
> - 总后台地址: <a target="_blank" href="https://malldemo.sdwanyue.com/admin/login/index">https://malldemo.sdwanyue.com/admin/login/index</a> 账号：visitor     密码：visitor
> - 商户后台地址: <a target="_blank" href="https://malldemo.sdwanyue.com/shop/index">https://malldemo.sdwanyue.com/shop/index</a> 账号：15711449029 验证码：123456（点击获取验证码后，输入该验证码即可）
> 
> ### 演示APP
> ![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_zhibo_ios/raw/master/zhibo_demo20230317.png)
> 
> ### 用户端仓库地址（可直接跳转查看）
>  - ios版仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/wanyue_zhibo_ios">点击此处</a>
>  - android仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/wanyue_zhibo_android">点击此处</a>
> 
>    
> ### 项目介绍
> 万岳科技可为商家快速搭建起一套属于自己的直播商城系统，有效避开商城直播过程中的痛点难点，加入自身创意的同时，汲取各家平台的特色功能和体验，并且可根据用户的运营需求对系统做定制开发。
> * 所有使用到的框架或者组件都是基于开源项目,代码保证100%开源。
> * 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的直播商城系统。
> 
> 
> ### 功能简介
![输入图片说明](https://images.gitee.com/uploads/images/2021/1101/173044_ae046c7e_8162876.png "直播电商开源版1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1101/173055_84f488a5_8162876.png "直播电商开源版2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1101/173103_5095e764_8162876.png "直播电商开源版3.png")
>  ### 功能介绍表
> ![输入图片说明](%E7%9B%B4%E6%92%AD%E5%B8%A6%E8%B4%A7%E7%B3%BB%E7%BB%9F%E5%8A%9F%E8%83%BD%E5%AF%B9%E6%AF%94%E8%A1%A8.png)
  
>  ### 开源版使用须知
>  - 需标注"代码来源于万岳科技开源项目"后即可免费自用运营
>  - 前端运营时展示的内容不得使用万岳科技相关信息
>  - 允许用于个人学习、教学案例
>  - 开源版不得直接倒卖源码
>  - 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负
>             
>   
> ###  开源交流群【加群回答请填写“gitee直播”，免费领取数据库文件】
>   咨询了解QQ：2770722087  微信：wanyuejiaoyu123
> > QQ群：995910672  QQ群：681418688
> > > ![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_education_web/raw/master/%E4%B8%87%E5%B2%B3%E7%A7%91%E6%8A%80%E5%BC%80%E6%BA%90%E8%AE%A8%E8%AE%BA10%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)  ![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_education_web/raw/master/%E4%B8%87%E5%B2%B3%E7%A7%91%E6%8A%80%E5%BC%80%E6%BA%90%E8%AE%A8%E8%AE%BA15%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
> 
 