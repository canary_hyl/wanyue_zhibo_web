<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\live;

use wanyue\basic\BaseModel;
use app\Redis;

/**
 * TODO 直播分类Model
 * Class StoreCart
 * @package app\models\store
 */
class LiveReportCat extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_report_cat';

    //列表
    public static function getList()
    {
        $key='live_report_cat';
        $list=Redis::get($key);
        if (!$list) {
            $list=self::field('id,name')->order('sort desc')->select()->toArray();
            if($list){
                Redis::set($key, $list);
            }else{
                Redis::del($key);
            }
        }
        return $list;
    }

}