<?php


namespace app\models\live;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;

/**
 * TODO 礼物Model
 * Class Gift
 * @package app\models\user
 */
class Gift extends BaseModel
{
    use ModelTrait;

    protected $pk = 'id';

    protected $name = 'gift';

    protected $insert = ['add_time'];

    protected $hidden = [
        'add_time',
    ];

    protected function setAddTimeAttr($value)
    {
        return time();
    }

    public static function setInfo($data)
    {
        return self::create($data);
    }

    /**
     * TODO 获取礼物信息
     * @param $id $id 编号
     * @param string $field $field 查询的字段
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getInfo($id, $field = '')
    {
        if (strlen(trim($field))) $info = self::where('id', $id)->field($field)->find();
        else  $info = self::where('id', $id)->find();
        if (!$info) return [];
        return $info->toArray();
    }

    /**
     * 更新
     * @param $data 更新信息数组
     * @param $id
     * @return bool
     */
    public static function updateInfo($data, $id)
    {
        return self::edit($data, $id, 'id');
    }

    /**
     * 获取团队信息
     * @param $uid
     * @param string $orderBy
     * @param string $keyword
     * @param int $page
     * @param int $limit
     * @return array
     */
    public static function getList($where='', $page = 0, $limit = 0, $orderBy = '' )
    {
        $model = new self;
        if ($orderBy === '') $orderBy = 'sort desc';
        if ($where !== '') $model = $model->where($where);

        $model = $model->field("id,type,name,coin,icon,swf,swftime,swftype");
        $model = $model->order($orderBy);
        if($page !=0 && $limit!=0) $model = $model->page($page, $limit);
        $list = $model->select();
        if ($list) return $list->toArray();
        else return [];
    }


}