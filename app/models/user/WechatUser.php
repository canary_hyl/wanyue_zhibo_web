<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/21
 */

namespace app\models\user;

use wanyue\basic\BaseModel;
use wanyue\traits\ModelTrait;

/**
 * TODO 微信用户Model
 * Class WechatUser
 * @package app\models\user
 */
class WechatUser extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'wechat_user';

    use ModelTrait;

    /**
     * 创建用户后返回uid
     * @param $routineInfo
     * @return mixed
     */
    public static function routineOauth($routine)
    {
        $routineInfo['nickname'] = filter_emoji(urldecode($routine['nickName']));//姓名
        $routineInfo['sex'] = $routine['gender'];//性别
        $routineInfo['language'] = $routine['language'];//语言
        $routineInfo['city'] = $routine['city'];//城市
        $routineInfo['province'] = $routine['province'];//省份
        $routineInfo['country'] = $routine['country'];//国家
        $routineInfo['headimgurl'] = urldecode($routine['avatarUrl']);//头像
        $routineInfo['openid'] = $routine['openId'];//openid
        $routineInfo['routine_openid'] = $routine['routine_openid'];//openid
        $routineInfo['session_key'] = $routine['session_key'];//会话密匙
        $routineInfo['unionid'] = $routine['unionId'];//用户在开放平台的唯一标识符
        $routineInfo['user_type'] = isset($routine['user_type']) ? $routine['user_type'] : 'routine';//用户类型

        if($routineInfo['nickname']==''){
            $routineInfo['nickname']='用户'.substr(md5( time()), 0, 4);
        }
        if($routineInfo['headimgurl']==''){
            $routineInfo['headimgurl']=sys_config('h5_avatar');
        }

        //  判断unionid  存在根据unionid判断
        if ($routineInfo['unionid'] != '' && ($uid = self::where(['unionid' => $routineInfo['unionid']])->where('user_type', '<>', 'h5')->value('uid'))) {
			$nickname =   $routineInfo['nickname'];
			
            self::edit($routineInfo, $uid, 'uid');
			unset($routineInfo['nickname']);
			$routineInfo['nickname']=$nickname;

            if ($routine['login_type']) $routineInfo['login_type'] = $routine['login_type'];
            User::updateWechatUser($routineInfo, $uid);
        } else if ($routineInfo['routine_openid'] !='' && $uid = self::where(['routine_openid' => $routineInfo['routine_openid']])->where('user_type', '<>', 'h5')->value('uid')) { //根据小程序openid判断
			$nickname =   $routineInfo['nickname'];
					
            self::edit($routineInfo, $uid, 'uid');
			unset($routineInfo['nickname']);
			$routineInfo['nickname']=$nickname;
            if ($routine['login_type']) $routineInfo['login_type'] = $routine['login_type'];
            User::updateWechatUser($routineInfo, $uid);
        }else if ($routineInfo['openid'] !='' && $uid = self::where(['openid' => $routineInfo['openid']])->where('user_type', '<>', 'h5')->value('uid')) { //根据openid判断
            $nickname =   $routineInfo['nickname'];

            self::edit($routineInfo, $uid, 'uid');
            unset($routineInfo['nickname']);
            $routineInfo['nickname']=$nickname;
            if ($routine['login_type']) $routineInfo['login_type'] = $routine['login_type'];
            User::updateWechatUser($routineInfo, $uid);
        } else {
            $routineInfo['add_time'] = time();//用户添加时间
            $routineInfo = self::create($routineInfo);
            $res = User::setRoutineUser($routineInfo);
            $uid = $res->uid;
        }
        return $uid;
    }

}