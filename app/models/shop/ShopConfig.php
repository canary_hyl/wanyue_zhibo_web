<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\shop;

use wanyue\basic\BaseModel;
use think\facade\Cache;

/**
 * TODO 直播分类Model
 * Class StoreCart
 * @package app\models\store
 */
class ShopConfig extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'shop_config';

    public static function getC($uid )
    {
        $config=[];
        $config_uid=self::where('uid',$uid)->find();
        if($config_uid){
            $config=json_decode($config_uid['value'],true);
        }
        return $config;
    }


}