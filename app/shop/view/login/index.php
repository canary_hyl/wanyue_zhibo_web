<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow" />
    <title>商家管理系统</title>
    <link href="{__FRAME_PATH}css/bootstrap.min.css?v=3.4.0" rel="stylesheet">
    <link href="{__PLUG_PATH}layui/css/layui.css" rel="stylesheet">
    <link href="{__FRAME_PATH}css/font-awesome.min.css?v=4.3.0" rel="stylesheet">
    <link href="{__FRAME_PATH}css/animate.min.css" rel="stylesheet">
    <link href="{__MODULE_PATH2}css/login.css?t=1594002976" rel="stylesheet">
    <script>
        top != window && (top.location.href = location.href);
    </script>
</head>
<body class="gray-bg login-bg">
    <div class="logo">
        <img src="/logo.png"> {:sys_config('site_name')}
    </div>
    <div class="login">
        <div class="login_left">
            <div class="login_img">
                <img src="{__MODULE_PATH2}images/login/login_img.png">
            </div>
        </div>
        <div class="login_right">
            <div class="login_bd">
                <div class="bd_title">
                    {:sys_config('site_name')}<br>
                    商家管理平台
                </div>
                <div class="bd_input">
                    <input type="text" id="account" placeholder="请输入手机号" autocomplete="off">
                </div>
                <div class="bd_input bd_code">
                    <input type="text" id="captcha" placeholder="请输入验证码" autocomplete="off">
                    <div class="getcode">获取验证码</div>
                </div>
                <div class="bd_submit">
                    <div class="submitbtn">登录</div>
                </div>
                <div class="bd_third">
                    <ul>
                        <li>
                            <a href="/shop/login/weixin">
                                <div class="bd_third_img">
                                    <img src="{__MODULE_PATH2}images/wx.png">
                                </div>
                                <div class="bd_third_name">微信登录</div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="login_bom">
        {:sys_config('site_name')}
    </div>
<!-- 全局js -->
<script src="{__PLUG_PATH}jquery-1.10.2.min.js"></script>
<script src="{__PLUG_PATH}layui/layui.all.js"></script>
<script src="{__MODULE_PATH2}js/login.js"></script>
<!--统计代码，可删除-->
<!--点击刷新验证码-->
</body>
</html>