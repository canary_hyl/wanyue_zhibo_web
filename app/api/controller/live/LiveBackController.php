<?php

namespace app\api\controller\live;

use app\models\live\Live;
use app\models\live\LiveRecord;
use app\Request;
use wanyue\services\UtilService;

class LiveBackController
{

    /**
     * 腾讯回调
     */
    /*
		回调数据格式
		{
				"channel_id": "2121_15919131751",
				"end_time": 1473125627,
				"event_type": 100,
				"file_format": "flv",
				"file_id": "9192487266581821586",
				"file_size": 9749353,
				"sign": "fef79a097458ed80b5f5574cbc13e1fd",
				"start_time": 1473135647,
				"stream_id": "2121_15919131751",
				"t": 1473126233,
				"video_id": "200025724_ac92b781a22c4a3e937c9e61c2624af7",
				"video_url": "http://200025724.vod.myqcloud.com/200025724_ac92b781a22c4a3e937c9e61c2624af7.f0.flv"
		}
	*/
    public function back_tx(Request $request)
    {

        $data=$request->param();

        $this->callbacklog('callback request:'.json_encode($data));

        if(!$data){
            $this->callbacklog("request para json format error");
            $result['code']=4001;
            echo json_encode($result);
            exit;
        }

        if(/* array_key_exists("t",$data) && array_key_exists("sign",$data) &&  */array_key_exists("event_type",$data)  && array_key_exists("stream_id",$data))
        {
            // $check_t = $data['t'];
            // $check_sign = $data['sign'];
            $event_type = $data['event_type'];
            $stream_id = $data['stream_id'];
        }else {
            $this->callbacklog("request para error");
            $result['code']=4002;
            echo json_encode($result);
            exit;
        }
        /* $md5_sign = $this-> GetCallBackSign($check_t);
        if( !($check_sign == $md5_sign) ){
            $this->callbacklog("check_sign error:" . $check_sign . ":" . $md5_sign);
            $result['code']=4003;
            echo json_encode($result);
            exit;
        }      */

        if($event_type == 100){
            /* 回放回调 */
            if(array_key_exists("video_id",$data) &&
                array_key_exists("video_url",$data) &&
                array_key_exists("start_time",$data) &&
                array_key_exists("end_time",$data) ){

                $video_id = $data['video_id'];
                $video_url = $data['video_url'];
                $start_time = $data['start_time'];
                $end_time = $data['end_time'];
            }else{
                $this->callbacklog("request para error:回放信息参数缺少" );
                $result['code']=4002;
                echo json_encode($result);
                exit;
            }
        }
        $ret=0;
        if($event_type == 0){
            /* 状态回调 断流 */
            $this->upOfftime(1,'',$stream_id);
        }elseif ($event_type == 1){
            /* 推流 */
            $this->upOfftime(0,'',$stream_id);
        }elseif ($event_type == 100){
            LiveRecord::setUrl($stream_id,$video_url);
        }

        $this->callbacklog('ret:'.json_encode($ret));
        $result['code']=$ret;
        echo json_encode($result);
        exit;
    }

    protected function GetCallBackSign($txTime){
        $live_push_key = sys_config('tx_push_key');
        $md5_val = md5($live_push_key . strval($txTime));
        return $md5_val;
    }

    protected function callbacklog($msg){
        file_put_contents(app()->getRootPath().'runtime/liveback_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').' 提交参数信息 :'.$msg."\r\n",FILE_APPEND);
    }

    protected function upOfftime($isoff=1,$uid='',$stream=''){
        $model=Live::where('islive',1);

        if($stream){
            $stream_a=explode('_',$stream);
            $uid = isset($stream_a[0])? $stream_a[0] : '0';
            $showid = isset($stream_a[1])? $stream_a[1] : '0';
        }

        if($showid){
            $model=$model->where('uid',$uid)->where('showid',$showid);
        }else{
            $model=$model->where('uid',$uid);
        }

        $data=[
            'isoff'=>$isoff,
            'offtime'=>0,
        ];
        if($isoff==1){
            $data['offtime']=time();
        }

        $model->update($data);

        return 0;
    }

    /* 定时处理关播-允许短时间 断流续推 */
    public function uplive(){
        $notime=time();

        $offtime=$notime - 30;

        $list=Live::where('islive','=','1')->where('isvideo','=','0')->where('isoff','=','1')->where('offtime','<',$offtime)->select();

        if(!$list) {
            echo 'OK';
            exit;
        }
        $list=$list->toArray();
        foreach ($list as $k=>$v){
            Live::stopLive($v['uid'],$v['showid']);
        }

        echo 'OK';
        exit;
    }



}