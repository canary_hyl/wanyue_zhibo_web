<?php

namespace app\api\controller\live;

use app\models\live\LiveReport;
use app\models\live\LiveReportCat;
use app\Request;
use wanyue\services\UtilService;

class LiveReportController
{

    //举报类型
    public function reportcat(Request $request)
    {

        $list = LiveReportCat::getList();

        return app('json')->successful($list);

    }

    /**
     * 举报
     */
    public function report(Request $request)
    {

        list($touid, $content) = UtilService::postMore([
            ['touid',0],
            ['content',''],
        ], $request, true);

        if($touid<1 || $content=='') return app('json')->fail('参数错误');

        $res = LiveReport::setReport($request->uid(), $touid,$content);
        if (!$res) return app('json')->fail(LiveReport::getErrorInfo());

        return app('json')->successful('举报成功');
    }



}