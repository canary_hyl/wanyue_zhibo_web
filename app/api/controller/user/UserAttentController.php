<?php
namespace app\api\controller\user;

use app\models\user\UserAttent;
use app\Request;
use wanyue\services\UtilService;

/**
 * 会员关注类
 */
class UserAttentController
{

    /**
     * 关注列表
     */
    public function getfollow(Request $request)
    {
        list($touid, $page, $limit) = UtilService::getMore([
            [['touid', 'd'], 0],
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);

        if($touid<1) return app('json')->successful([]);

        $uid=$request->uid();

        $list=UserAttent::getFollow($touid,$page,$limit);

        return app('json')->successful($list);
    }

    /**
     * 粉丝列表
     */
    public function getfans(Request $request)
    {
        list($touid,$page, $limit) = UtilService::getMore([
            [['touid', 'd'], 0],
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);

        if($touid<1) return app('json')->successful([]);

        $uid=$request->uid();

        $list=UserAttent::getFans($touid,$page,$limit);

        return app('json')->successful($list);
    }

    /**
     * 关注
     */
    public function add(Request $request)
    {
        list($touid) = UtilService::getMore([
            [['touid', 'd'], 0],
        ], $request, true);


        $uid=$request->uid();

        $res=UserAttent::addAttent($uid,$touid);

        if (!$res) return app('json')->fail(UserAttent::getErrorInfo());

        return app('json')->successful('已关注',['isattent'=>1]);
    }

    //取消关注
    public function del(Request $request)
    {
        list($touid) = UtilService::getMore([
            [['touid', 'd'], 0],
        ], $request, true);


        $uid=$request->uid();

        $res=UserAttent::delAttent($uid,$touid);

        if (!$res) return app('json')->fail(UserAttent::getErrorInfo());

        return app('json')->successful('已取关',['isattent'=>0]);
    }

}