<?php
namespace app\api\controller\cash;

use app\models\store\StoreOrder;
use app\models\user\User;
use app\models\cash\Shop;
use app\Request;
use wanyue\services\UtilService;

/**
 * 店铺提现
 */
class ShopController
{
    /**
     * 统计
     */
    public function info(Request $request)
    {

        $uid=$request->uid();

        $userinfo=User::getUserInfo($uid,'shop,shop_ok');

        $shop=$userinfo['shop'];

        $shop_t=StoreOrder::getTodaySettle($uid,1);

        $shop_ok=$userinfo['shop_ok'];
        $shop_no=StoreOrder::getUnSettle($uid,1);

        $shop_total=bcadd($shop_ok,$shop_no,2);

        return app('json')->successful(compact('shop','shop_t','shop_total','shop_ok','shop_no'));
    }

    /**
     * 列表
     */
    public function lst(Request $request)
    {
        list( $page, $limit) = UtilService::getMore([
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);

        $uid=$request->uid();

        $list=Shop::getList($uid,$page,$limit);

        return app('json')->successful($list);
    }

    //提现
    public function cash(Request $request)
    {
        list($money,$accountid) = UtilService::postMore([
            ['money', 0],
            [['accountid', 'd'], 0],
        ], $request, true);

        if($money<=0 || $accountid<1) return app('json')->fail('参数错误');
        $uid=$request->uid();

        $res=Shop::cash($uid,$money,$accountid);

        if (!$res) return app('json')->fail(Shop::getErrorInfo());

        return app('json')->successful('提交成功');
    }

    /**
     * 店铺收益结算记录
     */
    public function settlelst(Request $request)
    {
        list( $page, $limit) = UtilService::getMore([
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);

        $uid=$request->uid();

        $where=[
            'mer_id'=>$uid,
        ];

        $field='order_id,shop_income as money,settle_time';
        $list=StoreOrder::getUserSettleList($where,$field,$page,$limit);

        return app('json')->successful($list);
    }

}