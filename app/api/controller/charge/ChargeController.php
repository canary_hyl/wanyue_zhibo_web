<?php

namespace app\api\controller\charge;

use app\models\user\User;
use app\models\user\ChargeRules;
use app\models\charge\Charge;
use app\Request;
use app\Redis;
use wanyue\services\UtilService;
use wanyue\repositories\OrderRepository;

class ChargeController
{

    //充值信息
    public function getinfo(Request $request)
    {
		$uid=$request->uid();
    //    $uid=22;
		//充值列表
		$list = ChargeRules::getlist();
		//充值方式
		$config=[
			'aliapp_switch'=>sys_config('aliapp_switch'),
			'aliapp_partner'=>sys_config('aliapp_partner'),
			'aliapp_seller_id'=>sys_config('aliapp_seller_id'),
			'aliapp_key_android'=>sys_config('aliapp_key_android'),
			'aliapp_key_ios'=>sys_config('aliapp_key_ios'),
			'wx_switch'=>sys_config('wx_switch'),
			'wx_appid'=>sys_config('wx_app_appid'),
			'wx_appsecret'=>sys_config('wx_app_appsecret'),
			'wx_mchid'=>sys_config('wx_app_mchid'),
			'wx_key'=>sys_config('wx_app_apikey'),
		];
		//当前钻石余额
		$userinfo = User::getUserInfo($uid,'coin');
		$coin = $userinfo['coin'];
		//用户充值协议
		$h5url ='http://testsc.sdwanyue.com/appapi/page/detail?id=3';
		return app('json')->successful(compact('coin', 'list','config','h5url'));
    }
	//生成充值订单
	
    //充值微信
    public function getorderbywx(Request $request)
    {
		$uid=$request->uid();
        list($ruleid, $type, $coin,$money) = UtilService::postMore([
            [['ruleid', 'd'],0],
            [['type', 'd'],1],
            [['coin', 'd'], 0],
            ['money', ''],
        ], $request, true);
		
	/* 	$ruleid=1;
		$type=0;
		$coin=60;
		$money=0.01;
		$uid=22;
		 */
		$orderinfo = Charge::createorder($ruleid,$uid,$coin,$money,$type);
		
		if($orderinfo===1){ 
			return app('json')->fail('参数错误');
		}
		else{//生成订单成功，走支付流程，返回支付信息
			$jsConfig = OrderRepository::AppPayShop($uid, $orderinfo['orderno'],$money,'coincharge'); //创建订单jspay
			$info=$jsConfig;
			$info['orderno']=$orderinfo['orderno'];
		//	return app('json')->status('wechat_pay', '订单创建成功', $info);
			return app('json')->successful($info);
		}
    }	
    //充值微信
    public function getorderbyali(Request $request)
    {
		$uid=$request->uid();
        list($ruleid, $type, $coin,$money) = UtilService::postMore([
            [['ruleid', 'd'],0],
            [['type', 'd'],0],
            [['coin', 'd'], 0],
            ['money', ''],
        ], $request, true);
		
	/* 	$ruleid=1;
		$type=0;
		$coin=60;
		$money=0.01;
		$uid=22;
		 */
		$orderinfo = Charge::createorder($ruleid,$uid,$coin,$money,$type);
		
		if($orderinfo===1){ 
			return app('json')->fail('参数错误');
		}
		else{//生成订单成功，走支付流程，返回支付信息
			$info['orderno']=$orderinfo['orderno'];
			return app('json')->status('wechat_pay', '订单创建成功', $info);
		}
    }		
	

}