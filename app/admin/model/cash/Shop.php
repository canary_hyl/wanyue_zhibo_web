<?php

namespace app\admin\model\cash;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\models\user\User;


class Shop extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cash_shop';

    use ModelTrait;

    public static $status = [-1=>'拒绝',0=>'申请中',1=>'通过'];

    public static $types = [1=>'支付宝',2=>'微信',3=>'银行卡'];

    /**
     * 获取连表MOdel
     * @param $model
     * @return object
     */
    public static function getModelObject($where = [])
    {
        $model = new self();
        if (!empty($where)) {
            if (isset($where['uid']) && $where['uid'] != '') {
                $model = $model->where('uid',  $where['uid']);
            }
        }
        return $model;
    }

    /**
     * 异步获取列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $model = self::getModelObject($where);
        if(isset($where['page']) && $where['page']) $model = $model->page((int)$where['page'], (int)$where['limit']);
        $data = ($data =$model->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {
            $nickname='';
            $avatar='';
            $userinfo=User::getUserInfo($item['uid'],'uid,nickname,avatar');
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }
            $item['nickname']=$nickname;
            $item['avatar']=$avatar;
            $item['addtime']=date('Y-m-d H:i:s',$item['addtime']);
            $item['status_txt']=self::$status[$item['status']];

            $item['info']=self::$types[$item['type']].'<br>'.$item['account'].'<br>'.$item['name'].'<br>'.$item['bank'];
        }

        $count = self::getModelObject($where)->count();
        return compact('count', 'data');
    }

    public static function getInfo($id)
    {
        $data = self::where('id',$id)->find();
        if($data){
            $data=$data->toArray();

            $nickname='';
            $avatar='';
            $userinfo=User::getUserInfo($data['uid'],'uid,nickname,avatar');
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }
            $data['nickname']=$nickname;
            $data['avatar']=$avatar;
            $data['addtime']=date('Y-m-d H:i:s',$data['addtime']);
            $data['status_txt']=self::$status[$data['status']];

            $data['type_txt']=self::$types[$data['type']];
        }
        return $data;
    }


    public static function delid($id)
    {
        return self::del($id);
    }

}