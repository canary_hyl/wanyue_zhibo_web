<?php

namespace app\admin\controller\cash;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\cash\Gift as Model;
use app\admin\model\user\User as UserModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 礼物提现
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Gift extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['uid', '']
        ]);
        return Json::successlayui(Model::getList($where));
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $data = Model::getInfo($id);
        if (!$data) return Json::fail('数据不存在!');


        $this->assign(compact('data'));
        return $this->fetch();
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $data = Util::postMore([
            ['id', 0],
            ['status', 0],
        ], $request);
        $info = Model::get($data['id']);
        $data['uptime']=time();
        Model::edit($data, $data['id']);
        $status=$data['status'];

        if($status==-1 && $info['status']==0){
            UserModel::where('uid',$info['uid'])->inc('shop',$info['money'])->update();
        }

        return Json::successful('修改成功!');
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!Model::delid($id))
            return Json::fail(Model::getErrorInfo('删除失败,请稍候再试!'));
        else
            return Json::successful('删除成功!');
    }
}
