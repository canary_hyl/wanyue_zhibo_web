<?php

namespace app\admin\controller\live;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\live\ReportClass as ClassModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 直播举报分类
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Reportcat extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取分类列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['order', '']
        ]);
        return Json::successlayui(ClassModel::getList($where));
    }

    /**
     * 快速编辑
     * @param string $field
     * @param string $id
     * @param string $value
     */
    public function set_field($field = '', $id = '', $value = '')
    {
        $field == '' || $id == '' || $value == '' && Json::fail('缺少参数');
        if (ClassModel::where('id', $id)->update([$field => $value])){
            ClassModel::upCache();
            return Json::successful('保存成功');
        }else{
            return Json::fail('保存失败');
        }

    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $field = [
            Form::input('name', '名称'),
            Form::number('sort', '排序'),
        ];
        $form = Form::make_post_form('添加', $field, Url::buildUrl('save'), 2);
        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = Util::postMore([
            'name',
            'sort',
        ], $request);
        if (!$data['name']) return Json::fail('请输入名称');

        if ($data['sort'] < 0) $data['sort'] = 0;

        ClassModel::create($data);
        ClassModel::upCache();
        return Json::successful('添加成功!');
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $c = ClassModel::get($id);
        if (!$c) return Json::fail('数据不存在!');
        $field = [
            Form::input('name', '名称', $c->getData('name')),
            Form::number('sort', '排序',$c->getData('sort')),
        ];
        $form = Form::make_post_form('编辑', $field, Url::buildUrl('update', array('id' => $id)), 2);

        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $data = Util::postMore([
            'name',
            'sort',
        ], $request);
        if (!$data['name']) return Json::fail('请输入名称');
        if ($data['sort'] < 0) $data['sort'] = 0;
        ClassModel::edit($data, $id);
        ClassModel::upCache();
        return Json::successful('修改成功!');
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!ClassModel::delid($id)){
            return Json::fail(ClassModel::getErrorInfo('删除失败,请稍候再试!'));
        }
        ClassModel::upCache();
        return Json::successful('删除成功!');
    }
}
