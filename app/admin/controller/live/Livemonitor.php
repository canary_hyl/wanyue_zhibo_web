<?php

namespace app\admin\controller\live;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\live\Liveing as LiveingModel;
use wanyue\services\{ JsonService as Json, UtilService as Util};

/**
 * 直播监控控制器
 * @package app\admin\controller\system
 */
class Livemonitor extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {

        $this->assign([
            'chatserver'=>sys_config('chatserver'),
        ]);
        return $this->fetch();
    }

    public function getlist()
    {
        $where = Util::getMore([
            ['isvideo', 0],
            ['page', 1],
            ['limit', 5],
            ['order', '']
        ]);

        return Json::successlayui(LiveingModel::getLiveing($where));
    }

    /*
     *  关播
     *  @return json
     */
    public function stop()
    {
        $uid = $this->request->param('uid', 0);

        if($uid<1){
            return Json::fail('参数错误');
        }

        $info=LiveingModel::where('uid',$uid)->field('showid,islive')->find();

        if(!$info || $info['islive']!=1){
            return Json::fail('未查到直播记录');
        }

        $res=\app\models\live\Live::stopLive($uid,$info['showid']);
        if(!$res)  return Json::fail(\app\models\live\Live::getErrorInfo());

        return Json::successlayui('操作成功');
    }

}
