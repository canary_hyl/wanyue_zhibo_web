<?php

namespace app\admin\controller\shop;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\shop\ShopApply as Model;
use app\admin\model\user\User as UserModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

use app\admin\model\store\StoreProduct as ProductModel;

/**
 * 店铺申请
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Shopapply extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['keyword', '']
        ]);
        return Json::successlayui(Model::getList($where));
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $data = Model::get($id);
        if (!$data) return Json::fail('数据不存在!');

        $data['tel']=m_s($data['tel']);
        $data['cer_no']=m_s($data['cer_no']);

        $this->assign(compact('data'));
        return $this->fetch();
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $data = Util::postMore([
            ['uid', 0],
            ['status', 0],
            ['reason', ''],
        ], $request);

        $data['uptime']=time();
        Model::edit($data, $data['uid']);
        $isshop=0;
        if($data['status']==1){
            $isshop=1;
        }

        if($isshop==1){
            $data2=['isshop'=>1];
            UserModel::edit($data2,$data['uid']);
        }else{
            UserModel::where('uid',$data['uid'])->where('isshop',1)->update(['isshop'=>-1]);
        }

        //下架所有商品
        if($isshop==0){
            $where=['mer_id'=>$data['uid']];
            ProductModel::setUnshow($where);
        }

        return Json::successful('修改成功!');
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!Model::delApply($id)){
            return Json::fail(Model::getErrorInfo('删除失败,请稍候再试!'));
        }else{
            $data2=['isshop'=>0];
            UserModel::edit($data2,$id);
            //下架所有商品
            $where=['mer_id'=>$id];
            ProductModel::setUnshow($where);

            return Json::successful('删除成功!');
        }
    }
}
