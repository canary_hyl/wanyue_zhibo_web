<?php
/**
 * Created by PhpStorm.
 * User: xurongyao <763569752@qq.com>
 * Date: 2018/6/14 下午5:25
 */

namespace app\admin\controller\record;

use app\admin\controller\AuthController;
use app\admin\model\store\{StoreProduct};
use app\admin\model\order\StoreOrder;
use wanyue\services\{JsonService, UtilService as Util};
use app\admin\model\user\{User, UserBill, UserExtract};

/**
 * 微信充值记录
 * Class UserRecharge
 * @package app\admin\controller\user
 */
class Record extends AuthController
{
    /**
     * 显示操作记录
     */
    public function index()
    {


    }

    /**
     * 显示订单记录
     */
    public function chart_order()
    {
        $this->assign([
            'is_layui' => true,
            'year' => get_month()
        ]);
        return $this->fetch();
    }

    public function get_echarts_order()
    {
        $where = Util::getMore([
            ['type', ''],
            ['status', ''],
            ['data', ''],
        ]);
        return JsonService::successful(StoreOrder::getEchartsOrder($where));
    }

    /**
     * 显示产品记录
     */
    public function chart_product()
    {
        $this->assign([
            'is_layui' => true,
            'year' => get_month()
        ]);
        return $this->fetch();
    }

    /**
     * 获取产品曲线图数据
     */
    public function get_echarts_product($type = '', $data = '')
    {
        return JsonService::successful(StoreProduct::getChatrdata($type, $data));

    }

    /**
     * 获取销量
     */
    public function get_echarts_maxlist($data = '')
    {
        return JsonService::successful(StoreProduct::getMaxList(compact('data')));
    }

    /**
     * 获取利润
     */
    public function get_echarts_profity($data = '')
    {
        return JsonService::successful(StoreProduct::ProfityTop10(compact('data')));
    }

    /**
     * 获取缺货列表
     */
    public function getLackList()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
        ]);
        return JsonService::successlayui(StoreProduct::getLackList($where));
    }

    /**
     * 表单快速修改
     */
    public function editField($id = '')
    {
        $post = $this->request->post();
        StoreProduct::beginTrans();
        try {
            StoreProduct::edit($post, $id);
            StoreProduct::commitTrans();
            return JsonService::successful('修改成功');
        } catch (\Exception $e) {
            StoreProduct::rollbackTrans();
            return JsonService::fail($e->getMessage());
        }
    }

    //获取差评
    public function getnegativelist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 10],
        ]);
        return JsonService::successful(StoreProduct::getnegativelist($where));
    }

    /**
     * 获取退货
     */
    public function getTuiPriesList()
    {
        return JsonService::successful(StoreProduct::TuiProductList());
    }
    //营销统计

    /**
     * 显示积分统计
     */
    public function chart_score()
    {
        $this->assign([
            'is_layui' => true,
            'year' => get_month()
        ]);
        return $this->fetch();
    }

    /**
     * 获取积分头部信息
     */
    public function getScoreBadgeList($data = '')
    {
        return JsonService::successful(UserBill::getScoreBadgeList(compact('data')));
    }

    /**
     * 获取积分曲线图和柱状图
     */
    public function getScoreCurve($data = '', $limit = 20)
    {
        return JsonService::successful(UserBill::getScoreCurve(compact('data', 'limit')));
    }

    //会员统计


    //排行榜

    /**
     * 显示产品排行榜
     */
    public function ranking_saleslists()
    {
        $this->assign([
            'is_layui' => true,
        ]);
        return $this->fetch();
    }

    /*
     *获取产品排行 带分页
     */
    public function getSaleslists($start_time = '', $end_time = '', $title = '', $page = 1, $limit = 20)
    {
        return JsonService::successlayui(StoreProduct::getSaleslists(compact('start_time', 'end_time', 'title', 'page', 'limit')));
    }

    /*
     *生成表格,并下载
     */
    public function save_product_export($start_time = '', $end_time = '', $title = '')
    {
        return JsonService::successlayui(StoreProduct::SaveProductExport(compact('start_time', 'end_time', 'title')));
    }

    /*
     *获取单个商品的详情
     */
    public function product_info($id = '')
    {
        if ($id == '') $this->failed('缺少商品id');
        if (!StoreProduct::be(['id' => $id])) return $this->failed('商品不存在!');
        $this->assign([
            'is_layui' => true,
            'year' => get_month(),
            'id' => $id,
        ]);
        return $this->fetch();
    }

    /*
     *获取单个商品的详情头部信息
     */
    public function getProductBadgeList($id = '', $data = '')
    {
        return JsonService::successful(StoreProduct::getProductBadgeList($id, $data));
    }

    /*
     *获取单个商品的销售曲线图
     */
    public function getProductCurve($id = '', $data = '', $limit = 20)
    {
        return JsonService::successful(StoreProduct::getProductCurve(compact('id', 'data', 'limit')));
    }

    /*
     *获取单个商品的销售总条数
     */
    public function getProductCount($id, $data = '')
    {
        return JsonService::successful(StoreProduct::setWhere(compact('data'))
            ->where('a.product_id', $id)
            ->join('user c', 'c.uid=a.uid')
            ->where('a.is_pay', 1)
            ->count());
    }

    /*
     *获取单个商品的销售列表
     */
    public function getSalelList($data = '', $id = 0, $page = 1, $limit = 20)
    {
        return JsonService::successful(StoreProduct::getSalelList(compact('data', 'id', 'page', 'limit')));
    }

    /**
     * 显示积分排行榜
     */
    public function ranking_point()
    {
        $this->assign([
            'is_layui' => true,
            'year' => get_month()
        ]);
        return $this->fetch();
    }

    //获取所有积分排行总人数
    public function getPountCount()
    {
        return JsonService::successful(User::where('status', 1)->where('integral', '<>', 0)->count());
    }

    //获取积分排行列表
    public function getpointList($page = 1, $limit = 20)
    {
        return JsonService::successful(($list = User::where('status', 1)
            ->where('integral', '<>', 0)
            ->field('nickname,integral')
            ->order('integral desc')
            ->page($page, $limit)
            ->select()) && count($list) ? $list->toArray() : []);
    }

    //获取本月积分排行别表
    public function getMonthpountList($page = 1, $limit = 20)
    {
        return JsonService::successful(($list = User::where('status', 1)
            ->where('integral', '<>', 0)
            ->whereTime('add_time', 'month')
            ->order('integral desc')
            ->field('nickname,integral')
            ->page($page, $limit)
            ->select()) && count($list) ? $list->toArray() : []);
    }

    public function getMonthPountCount()
    {
        return JsonService::successful(User::where('status', 1)->where('integral', '<>', 0)->whereTime('add_time', 'month')->count());
    }

    /**
     *
     * 显示下级会员排行榜
     */
    public function ranking_lower()
    {
        echo " 复购率 复购增长率 活跃度 活跃率 分销总金额 增长率 消费会员 非消费会员 消费排行榜 积分排行榜 余额排行榜 分销总金额排行榜 分销人数排行榜 分销余额排行榜 购物金额排行榜 购物次数排行榜 提现排行榜 ";
    }


}

